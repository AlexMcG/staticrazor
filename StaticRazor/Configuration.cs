﻿using Newtonsoft.Json;
using System.Composition;
using System.IO;

namespace StaticRazor
{
    /// <summary>
    /// Configuration 
    /// 
    /// 
    /// Notes 
    ///     I don't think the JsonIgnore attributes are needed now since the portion of the class has 
    ///     been pulled out into another class. 
    ///     
    ///     I have a slight concern about the manual nature of determining whether or not we need to 
    ///     serialize the the data. I have to remember to call Serialize(). Surely there's a better way..
    ///     
    /// </summary>
    [Export(typeof(Configuration)), Shared]
    public class Configuration
    {
        public string WebsiteRoot
        {
            get
            {
                return _config.WebsiteRoot;
            }
            set
            {
                _config.WebsiteRoot = value;
                Seralize();
            }
        }
        public string Output
        {
            get
            {
                return _config.Output;
            }
            set
            {
                _config.Output= value;
                Seralize();
            }
        }

        public int BlogPostsPerPage
        {
            get
            {
                return _config.BlogPostsPerPage;
            }
            set
            {
                _config.BlogPostsPerPage= value;
                Seralize();
            }
        }

        public string LevelismURL
        {
            get
            {
                return _config.LevelismURL;
            }
            set
            {
                _config.LevelismURL = value;
                Seralize();
            }
        }

        public string AutoSerializePath { get; set; }

        // Non-serialized properties
        public string Title { get; set; }

        public int LevelismBlogPagesToSnake
        {
            get
            {
                return _config.LevelismBlogPagesToSnake;
            }
            set
            {
                _config.LevelismBlogPagesToSnake = value;
                Seralize();
            }
        }

        [JsonIgnore]
        public string CSS_UnselectedMenuItem => @"pure-menu-item";

        [JsonIgnore]
        public string CSS_SelectedMenuItem => @"pure-menu-item menu-item-divided pure-menu-selected";

        [JsonIgnore]
        public string Output_BlogPages => Path.Combine(Output, @"pages");

        [JsonIgnore]
        public string Output_TagMasterPage => Path.Combine(Output, @"tag_master_page.html");

        [JsonIgnore]
        public string Output_ArticlesPage => Path.Combine(Output, @"articles.html");

        [JsonIgnore]
        public string MenuTemplateFilename => Path.Combine(TemplatePath, @"menu.html");

        [JsonIgnore]
        public string TemplatePath => Path.Combine(WebsiteRoot, "templates");

        [JsonIgnore]
        public string CSSSourcePath => Path.Combine(TemplatePath, "css_source");

        [JsonIgnore]
        public string CSSOutputPath => Path.Combine(TemplatePath, "css\\layouts");

        [JsonIgnore]
        public string JSPath => Path.Combine(TemplatePath, "js");

        [JsonIgnore]
        public string BlogPath => Path.Combine(WebsiteRoot, "blog");

        [JsonIgnore]
        public string GamesPath => Path.Combine(WebsiteRoot, "games");

        [JsonIgnore]
        public string LevelDesignPath => Path.Combine(WebsiteRoot, "level_design");

        [JsonIgnore]
        public string ArtPath => Path.Combine(WebsiteRoot, "art");

        [JsonIgnore]
        public string AboutPath => Path.Combine(WebsiteRoot, "about");

        [JsonIgnore]
        public string ProgrammingProjectsPath => Path.Combine(WebsiteRoot, "programming_projects");

        [JsonIgnore]
        public string ResourcesPath => Path.Combine(WebsiteRoot, "resources");

        [JsonIgnore]
        public string ColorTemplatePath => Path.Combine(WebsiteRoot, "colors");

        [JsonIgnore]
        public string ImageResourcesPath => Path.Combine(ResourcesPath, "images");

        [JsonIgnore]
        public string ImageResourcesURL => "resources/images";

        [JsonIgnore]
        public string LoggerName { get; set; } = "StaticRazorApplication";

        /// <summary>
        /// TODO Add some logic to see if the file exists and if not create it.
        /// </summary>
        [JsonIgnore]
        public string DefaultColorTemplate => Path.Combine(ColorTemplatePath, "default.txt");

        private void Seralize()
        {
            if (!string.IsNullOrEmpty(AutoSerializePath))
            {
                Serialize(AutoSerializePath);
            }
        }

        public void Serialize(string path)
        {
            string serializedObject = JsonConvert.SerializeObject(_config, Formatting.Indented);
            File.WriteAllText(path,serializedObject);
        }

        public void Deserialize(string path)
        {
            string serializedObject = File.ReadAllText(path);
            _config = JsonConvert.DeserializeObject<ConfigurationImplementation>(serializedObject);
        }

        private ConfigurationImplementation _config = new ConfigurationImplementation();
    }

    class ConfigurationImplementation
    {
        // Serialized properties
        public string WebsiteRoot { get; set; }
        public string Output { get; set; }
        public int BlogPostsPerPage { get; set; } = 3;
        public string LevelismURL { get; set; }
        public int LevelismBlogPagesToSnake { get; set; } = -1;
    }
}