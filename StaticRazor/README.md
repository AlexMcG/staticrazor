
# Overview

This is a static site generator I made for pretty much my own purposes. It has very specific features such as exporter to convert my old wordpress blog posts into markdown.

Essentially I figured it would take a similar amount of time to configure an existing static site generator to fit my needs as it would to write something custom so I decided to write something custome because it's much more fun.


