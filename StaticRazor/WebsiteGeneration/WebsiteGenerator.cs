﻿using RazorEngine;
using RazorEngine.Templating;
using StaticRazor.Model;
using StaticRazor.Services;
using StaticRazor.ViewModels;
using System;
using System.Composition;
using System.IO;
using System.Threading.Tasks;

namespace StaticRazor.WebsiteGeneration
{
    /// <summary>
    /// 
    /// NOTE1
    /// 
    /// Task.WhenAll appears to not call the next method in its params until the 
    /// first method awaits.Since these methods are long running this means that
    /// in this case Task.WhenAll runs sequentially.Yielding to the caller immidiately
    /// fixes this.
    /// 
    /// </summary>
    [Export]
    public class WebsiteGenerator
    {
        [ImportingConstructor]
        public WebsiteGenerator(
            Configuration config,
            ILoggingService loggingService,
            ColorTemplateApplicator colorTemplateApplicator,
             HtmlAttributeInjector attributeInjector,
             MetaDataParsingService postMetaDataService,
             MenuManipulator menuManipulator,
             YouTubeService youtubeService)
        {
            _config = config;
            _loggingService = loggingService;
            _colorTemplateApplicator = colorTemplateApplicator;
            _attributeInjector = attributeInjector;
            _postMetaDataService = postMetaDataService;
            _menuManipulator = menuManipulator;
            _viewModel = new Lazy<WebsiteViewModel>
                (() => new WebsiteViewModel(_config, _postMetaDataService, _loggingService, youtubeService));

            _attributeInjector.ClearAttributes();
            _attributeInjector.AddAttribute("descendant::img", "class", "pure-img-responsive");
            _attributeInjector.AddAttribute("descendant::iframe", "max-width", "100%");
            _attributeInjector.AddAttribute("descendant::iframe", "max-height", "562.5");
            _attributeInjector.AddAttribute("descendant::iframe", "width", "100%");
            _attributeInjector.AddAttribute("descendant::iframe", "height", "562.5");

        }

        /// <summary>
        /// Couple different places generate or reference blog pages. Use this template and make sure the 
        /// numerical formatting matches!
        /// </summary>
        public static string BlogPageNameStringTemplate = "blog_page_{0}.html";

        public void GenerateWebsite(string outputPath, bool cleanOutput = true)
        {
            _loggingService.Info("Generating website");
            if (cleanOutput)
            {
                if (!CleanOutput(outputPath))
                {
                    _loggingService.Error("Unable prepare output folder. Cancelling work.");
                    return;
                }
            }

            try
            {
                Task.WaitAll(
                    Task.Run(() => GenerateBlogPosts(outputPath)),
                    Task.Run(() => GenerateBlogListOrderedByDate(outputPath)),
                    Task.Run(() => GenerateIndividualPages(outputPath)),
                    Task.Run(() => GenerateTagPages(outputPath)),
                    Task.Run(() => GenerateTagMasterPage(outputPath)),
                    Task.Run(() => GenerateGamePages(outputPath)),
                    Task.Run(() => GenerateGameMasterPage(outputPath)),
                    Task.Run(() => GenerateLevelDesignMasterPage(outputPath)),
                    Task.Run(() => GenerateLevelDesignPages(outputPath)),
                    Task.Run(() => GenerateArtPage(outputPath)),
                    Task.Run(() => GenerateProgrammingProjectsPage(outputPath)),
                    Task.Run(() => GenerateAboutPage(outputPath)),
                    Task.Run(() => GenerateArticlesPage(outputPath))
                    );

                // TODO generate this in parallel
                CopyTemplateResources(outputPath);
            }
            catch (Exception ex)
            {
                _loggingService.Error(ex);
            }

            _loggingService.Info("Generating website complete");
        }

        private string GetTemplateContent(string templatePath, string menuSection, bool shouldInjectMenuIntoTemplate = true)
        {
            string InjectMenuContentIntoTemplate(
                string templateHtml,
                string menuHtmlFragmentContent,
                string replacementSymbol = @"<!--MENU_GETS_INJECTED_HERE-->")
                    => templateHtml.Replace(replacementSymbol, menuHtmlFragmentContent);

            var templateHtmlContent = File.ReadAllText(templatePath);
            var menuHtmlContent = File.ReadAllText(_config.MenuTemplateFilename);
            var menuHtmlContentWithSelectedSectionApplied = _menuManipulator.GenerateMenuHTMLFragment(
                menuHtmlContent, 
                menuSection, 
                _config.CSS_UnselectedMenuItem, 
                _config.CSS_SelectedMenuItem);
            var finalTemplateHtmlContent = shouldInjectMenuIntoTemplate ?
                    InjectMenuContentIntoTemplate(templateHtmlContent, menuHtmlContentWithSelectedSectionApplied) :
                    templateHtmlContent;

            return finalTemplateHtmlContent;
        }

        bool CleanOutput(string outputPath)
        {
            _loggingService.Info("Cleaning output directory");
            const int Tries = 10;
            int triesLeft = Tries;
            while (triesLeft > 0)
            {
                triesLeft--;
                try
                {
                    if (Directory.Exists(outputPath))
                    {
                        Directory.Delete(outputPath, true);
                    }
                    else
                    {
                        break;
                    }
                }
                // dont log the exception until we define verbose channels (ex WarningVerbose)
                //catch (Exception ex)
                catch 
                {
                    _loggingService.Warning("unable to delete the directory. " + triesLeft + " tries left.");
                    //_loggingService.Error(ex);
                }
            }
            if (triesLeft == 0)
            {
                _loggingService.Error("unable to clean output directory after " + Tries + " tries");
                return false;
            }
            try
            {
                Directory.CreateDirectory(outputPath);
            }
            catch (Exception ex)
            {
                _loggingService.Error(ex);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Copies css and img files from the templates folder to the final output
        /// </summary>
        /// <param name="website"></param>
        /// <param name="outputPath"></param>
        private void CopyTemplateResources(string outputPath)
        {
            string defaultColorTemplatePath = _config.ColorTemplatePath + "\\" + "default.txt";
            _colorTemplateApplicator
                .ApplyTemplate(defaultColorTemplatePath, _config.CSSSourcePath, _config.CSSOutputPath);

            _loggingService.Info("Copying templates");

            var cssDir = Directory.CreateDirectory(outputPath + @"\css");
            var jsDir = Directory.CreateDirectory(outputPath + @"\js");
            var resDir = Directory.CreateDirectory(outputPath + @"\resources");

            FileSystemUtil.DirectoryCopyRecurisive(_config.TemplatePath + @"\css", cssDir.FullName);
            FileSystemUtil.DirectoryCopyRecurisive(_config.TemplatePath + @"\js", jsDir.FullName);
            FileSystemUtil.DirectoryCopyRecurisive(_config.ResourcesPath, resDir.FullName);
        }

        private void GenerateTagPages(string outputPath)
        {
            _loggingService.Info("Generating tag pages.");
            bool hasErrors = false;

            string postTemplate = GetTemplateContent(_config.TemplatePath + @"\tag_page.html","blog");

            var templateKey = RazorTemplateKeyPostfixService.GetTemplateKey("tagPageTemplateKey");
            Parallel.ForEach(ViewModel.TagPages.Values, (tagPage) =>
            {
                string formattedFileName = outputPath + @"\" + tagPage.FileName;

                try
                {
                    var result = Engine.Razor.RunCompile(
                            postTemplate,
                            templateKey,
                            typeof(TagViewModel),
                            tagPage);

                    File.WriteAllText(
                             formattedFileName,
                            _attributeInjector.InjectAttributesToHTML(result));
                }
                catch (Exception ex)
                {
                    _loggingService.Error(ex);
                    hasErrors = true;
                }
            }
            );

            if (hasErrors)
            {
                _loggingService.Info("Tag page generation completed with errors.");
            }
            else
            {
                _loggingService.Info("Tag page generation complete.");
            }
        }

        private void GenerateTagMasterPage(string outputPath)
        {
            _loggingService.Info("Generating tag master page.");

            string postTemplate = GetTemplateContent(_config.TemplatePath + @"\tag_master_page.html","blog");
            var templateKey = RazorTemplateKeyPostfixService.GetTemplateKey("tag_master_pagePageTemplateKey");

            try
            {
                var result = Engine.Razor.RunCompile(
                        postTemplate,
                        templateKey,
                        typeof(WebsiteViewModel),
                        ViewModel);

                File.WriteAllText(
                         _config.Output_TagMasterPage,
                        _attributeInjector.InjectAttributesToHTML(result));
            }
            catch (Exception ex)
            {
                _loggingService.Error(ex);
            }

            _loggingService.Info("Generating tag master page complete.");
        }

        private async Task GenerateBlogListOrderedByDate(string outputPath)
        {
            // see NOTE1
            await Task.Yield();

            _loggingService.Info("Generating blog list ordered by date.");
            if (!Directory.Exists(outputPath))
            {
                Directory.CreateDirectory(outputPath);
            }
            try
            {
                string template = GetTemplateContent(_config.TemplatePath + @"\blog_ordered_by_date.html","blog");

                var result = Engine.Razor.RunCompile(
                    template,
                    RazorTemplateKeyPostfixService.GetTemplateKey("blog_ordered_by_date"),
                    typeof(WebsiteViewModel),
                    ViewModel);

                File.WriteAllText(
                    outputPath + @"\blog_ordered_by_date.html",
                    _attributeInjector.InjectAttributesToHTML(result));
            }
            catch (Exception ex)
            {
                _loggingService.Error(ex);
            }
            _loggingService.Info("Generating blog list ordered by date complete!");
        }

        private async Task GenerateIndividualPages(string outputPath)
        {
            // see NOTE1
            await Task.Yield();

            _loggingService.Info("Generating blog individual pages.");

            try
            {
                string postTemplate = GetTemplateContent(_config.TemplatePath + @"\post.html","blog");
                //foreach (var post in _viewModel.Value.BlogPosts)
                var templateKey = RazorTemplateKeyPostfixService.GetTemplateKey("postTemplateKey");
                Parallel.ForEach(ViewModel.BlogPosts, (post) =>
                {
                    var result = Engine.Razor.RunCompile(
                        postTemplate,
                        templateKey,
                        typeof(BlogPostViewModel),
                        post);

                    string formattedBlogFileName = Path.GetFileNameWithoutExtension(post.FileName);

                    // NOTE : If I switch to .net core 2+ there is a function called WriteAllTextAsync which
                    // would make this easier to make truly concurrent... If Razor uses a lot of cpu though 
                    // maybe we do want this to be run on a separate thread? Will have to run some perf tests.
                    File.WriteAllText(
                        outputPath + @"\" + formattedBlogFileName + ".html",
                        _attributeInjector.InjectAttributesToHTML(result));
                }
                );
            }
            catch (Exception ex)
            {
                _loggingService.Error(ex);
            }
            _loggingService.Info("Generating blog individual pages complete!");
        }

        /// <summary>
        /// Generates all the blog post html pages with pagination.</summary>
        /// <param name="website"></param>
        /// <param name="outputPath"></param>
        private async Task GenerateBlogPosts(string outputPath)
        {
            // see NOTE1
            await Task.Yield();

            _loggingService.Info("Generating blog posts.");
            // generate blog html pages
            string blogTemplate = GetTemplateContent(_config.TemplatePath + @"\blog.html","blog");
            var templateKey = RazorTemplateKeyPostfixService.GetTemplateKey("blogTemplateKey");
            //foreach (var page in _viewModel.Value.BlogPages)
            Parallel.ForEach(ViewModel.BlogPages, (page) =>
            {
                var result = Engine.Razor.RunCompile(
                blogTemplate,
                templateKey,
                typeof(BlogPageViewModel),
                page);

                string formattedBlogFileName =
                    string.Format(BlogPageNameStringTemplate, page.PageNumber.ToString("0000"));

                File.WriteAllText(
                    outputPath + @"\" + formattedBlogFileName,
                    _attributeInjector.InjectAttributesToHTML(result));
            }
            );

            _loggingService.Info("Generating blog posts complete!");
        }

        private async Task GenerateGamePages(string outputPath)
        {
            await Task.Yield();
            _loggingService.Info("Generating game pages.");
            string gameTemplate = GetTemplateContent(_config.TemplatePath + @"\game.html","games");
            var templateKey = RazorTemplateKeyPostfixService.GetTemplateKey("gameTemplateKey");
            try
            {
                Parallel.ForEach(ViewModel.GamePages, (page) =>
                    {
                        var result = Engine.Razor.RunCompile(
                        gameTemplate,
                        templateKey,
                        typeof(GameModel),
                        page);

                        File.WriteAllText(outputPath + @"\" + page.FileNameHTML,result);
                    });
            }
            catch (Exception ex)
            {
                _loggingService.Error(ex);
            }
            _loggingService.Info("Generating game pages complete!");
        }

        private async Task GenerateGameMasterPage(string outputPath)
        {
            await Task.Yield();

            _loggingService.Info("Generating game master pages.");

                try
                {
                    string template = GetTemplateContent(_config.TemplatePath + @"\game_ordered_by_category.html","games");
                    var templateKey = RazorTemplateKeyPostfixService.GetTemplateKey("game_ordered_by_categoryTemplateKey");

                    var result = Engine.Razor.RunCompile(
                        template,
                        templateKey,
                        typeof(WebsiteViewModel),
                        ViewModel);

                    File.WriteAllText(
                        outputPath + @"\" + "game_ordered_by_category.html",
                        _attributeInjector.InjectAttributesToHTML(result));
                }
                catch (Exception ex)
                {
                    _loggingService.Error(ex);
                }

            _loggingService.Info("Generating game master pages complete!");
        }

        private async Task GenerateLevelDesignPages(string outputPath)
        {
            await Task.Yield();
            _loggingService.Info("Generating level design pages.");
            string levelDesignTemplate = GetTemplateContent(_config.TemplatePath + @"\level.html","level_design");
            var templateKey = RazorTemplateKeyPostfixService.GetTemplateKey("level_designTemplateKey");
            try
            {
                Parallel.ForEach(ViewModel.LevelDesignPages, (page) =>
                {
                    var result = Engine.Razor.RunCompile(
                    levelDesignTemplate,
                    templateKey,
                    typeof(LevelDesignModel),
                    page);

                    File.WriteAllText(outputPath + @"\" + page.FileNameHTML, result);
                });
            }
            catch (Exception ex)
            {
                _loggingService.Error(ex);
            }
            _loggingService.Info("Generating level design pages complete!");
        }

        private async Task GenerateLevelDesignMasterPage(string outputPath)
        {
            await Task.Yield();
            _loggingService.Info("Generating level design master page");

            try
            {
                string template = GetTemplateContent(_config.TemplatePath + @"\level_design_ordered_by_category.html","level_design");
                var templateKey = RazorTemplateKeyPostfixService.GetTemplateKey("level_design_ordered_by_categoryTemplateKey");

                var result = Engine.Razor.RunCompile(
                    template,
                    templateKey,
                    typeof(WebsiteViewModel),
                    ViewModel);

                File.WriteAllText(
                    outputPath + @"\" + "level_design_ordered_by_category.html",
                    _attributeInjector.InjectAttributesToHTML(result));
            }
            catch(Exception ex)
            {
                _loggingService.Error(ex);
            }

            _loggingService.Info("Generating level design master page complete!");
        }

        private async Task GenerateArtPage(string outputPath)
        {
            await Task.Yield();
            _loggingService.Info("Generating art page page");

            try
            {
                string template = GetTemplateContent(_config.TemplatePath + @"\art_ordered_by_date.html","art");
                var templateKey = RazorTemplateKeyPostfixService.GetTemplateKey("art_ordered_by_dateTemplateKey");

                var result = Engine.Razor.RunCompile(
                    template,
                    templateKey,
                    typeof(WebsiteViewModel),
                    ViewModel);

                File.WriteAllText(
                    outputPath + @"\" + "art_ordered_by_date.html",
                    _attributeInjector.InjectAttributesToHTML(result));
            }
            catch (Exception ex)
            {
                _loggingService.Error(ex);
            }

            _loggingService.Info("Generating art page complete!");
        }

        private async Task GenerateProgrammingProjectsPage(string outputPath)
        {
            await Task.Yield();
            _loggingService.Info("Generating programming page");

            try
            {
                string template = GetTemplateContent(_config.TemplatePath + @"\programming_ordered_by_date.html","programming");
                var templateKey = RazorTemplateKeyPostfixService.GetTemplateKey("programming_ordered_by_date");

                var result = Engine.Razor.RunCompile(
                    template,
                    templateKey,
                    typeof(WebsiteViewModel),
                    ViewModel);

                File.WriteAllText(
                    outputPath + @"\" + "programming_ordered_by_date.html",
                    _attributeInjector.InjectAttributesToHTML(result));
            }
            catch (Exception ex)
            {
                _loggingService.Error(ex);
            }

            _loggingService.Info("Generating programming page complete!");
        }

        private async Task GenerateAboutPage(string outputPath)
        {
            await Task.Yield();
            _loggingService.Info("Generating about page");

            try
            {
                string template = GetTemplateContent(_config.TemplatePath + @"\about.html","about");
                var templateKey = RazorTemplateKeyPostfixService.GetTemplateKey("about");

                var result = Engine.Razor.RunCompile(
                    template,
                    templateKey,
                    typeof(AboutModel),
                    ViewModel.AboutPage);

                File.WriteAllText(
                    outputPath + @"\" + "about.html",
                    _attributeInjector.InjectAttributesToHTML(result));
            }
            catch (Exception ex)
            {
                _loggingService.Error(ex);
            }

            _loggingService.Info("Generating about page complete!");
        }

        private void GenerateArticlesPage(string outputPath)
        {
            _loggingService.Info("Generating articles_page master page.");

            string postTemplate = GetTemplateContent(_config.TemplatePath + @"\articles_page.html","articles");
            var templateKey = RazorTemplateKeyPostfixService.GetTemplateKey("articles_pagePageTemplateKey");

            try
            {
                var result = Engine.Razor.RunCompile(
                        postTemplate,
                        templateKey,
                        typeof(WebsiteViewModel),
                        ViewModel);

                File.WriteAllText(
                         _config.Output_ArticlesPage,
                        _attributeInjector.InjectAttributesToHTML(result));
            }
            catch (Exception ex)
            {
                _loggingService.Error(ex);
            }

            _loggingService.Info("Generating articles_page master page complete.");
        }

        private WebsiteViewModel ViewModel => _viewModel.Value;

        private Configuration _config;
        private readonly ILoggingService _loggingService;
        private readonly ColorTemplateApplicator _colorTemplateApplicator;
        private HtmlAttributeInjector _attributeInjector;
        private MetaDataParsingService _postMetaDataService;
        private readonly MenuManipulator _menuManipulator;
        private readonly Lazy<WebsiteViewModel> _viewModel;
    }
}