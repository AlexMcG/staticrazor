﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Composition;

namespace StaticRazor.WebsiteGeneration
{
    [Export]
    public class HtmlAttributeInjector
    {
        public void AddAttribute(string node, string key, string value)
        {
            _attributeCollection.AddAttribute(node, key, value);
        }

        public void ClearAttributes() => _attributeCollection.Attributes.Clear();

        internal HtmlTagAttributeInjectionCollection AttributeCollection => _attributeCollection;

        /// <summary>
        /// Takes in a block of html and injects attributes to any nodes set to have 
        /// attributes injected.
        /// </summary>
        /// <param name="htmlContent"></param>
        /// <returns>Transformed html document with attributes injected</returns>
        public string InjectAttributesToHTML(string htmlContent)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(htmlContent);
            foreach (var nodeAttributeKvp in _attributeCollection.Attributes)
            {
                var nodes = doc.DocumentNode.SelectNodes(nodeAttributeKvp.Key);
                if (nodes == null)
                {
                    continue;
                }
                foreach (var htmlNode in nodes)
                {
                    foreach (var attributeKvp in nodeAttributeKvp.Value)
                    {
                        htmlNode.SetAttributeValue(attributeKvp.Key, attributeKvp.Value);
                    }
                }
            }
            return doc.DocumentNode.WriteContentTo();
        }

        private HtmlTagAttributeInjectionCollection _attributeCollection = 
            new HtmlTagAttributeInjectionCollection();

        internal class HtmlTagAttributeInjectionCollection
        {
            public HtmlTagAttributeInjectionCollection()
            {
                _attributes = new Dictionary<string, Dictionary<string, string>>();
            }

            public Dictionary<string, Dictionary<string, string>> Attributes => _attributes;

            public void AddAttribute(string nodeKey, string attributekey, string attributeValue)
            {
                if (_attributes.ContainsKey(nodeKey))
                {
                    var nodeAttributes = _attributes[nodeKey];
                    if (nodeAttributes.ContainsKey(attributekey))
                    {
                        throw new ArgumentException(
                            "The node " +
                            nodeKey +
                            " already has an attribute with the type " +
                            attributekey +
                            " set. No duplicates.");
                    }
                    else
                    {
                        nodeAttributes.Add(attributekey, attributeValue);
                    }
                }
                else
                {
                    var attrDict = new Dictionary<string, string>();
                    attrDict.Add(attributekey, attributeValue);
                    _attributes.Add(nodeKey, attrDict);
                }
            }

            /// <summary>
            ///  key is the node name EX : <b> or <div>
            ///  value is a hashmap of keyvalues pairs of attributes to be injected
            ///  EX ( b , ( text-align, center)) means the b tag will have the attributes 
            ///  text-aling:center injected into it.
            /// </summary>
            private readonly Dictionary<string, Dictionary<string, string>> _attributes;
        }
    }
}
