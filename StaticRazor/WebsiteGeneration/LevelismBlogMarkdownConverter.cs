﻿using HtmlAgilityPack;
using Newtonsoft.Json;
using StaticRazor.Model;
using StaticRazor.Services;
using System;
using System.Collections.Generic;
using System.Composition;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace StaticRazor.WebsiteGeneration
{
    public class WordpressGalleryConverter
    {
        public static string Clean(string input, ILoggingService loggingService)
        {
            string modified = input;
            {
                var matches = GalleryDivOpenerRegex.Matches(modified);

                foreach (Match match in matches)
                {
                    //loggingService.Warning("GalleryDivOpenerRegex string size pre match" + modified.Length);
                    modified = modified.Replace(match.Value, "");
                    //loggingService.Warning("GalleryDivOpenerRegex string size post match" + modified.Length);
                }
            }

            {
                var matches = DLOpenerRegex.Matches(modified);

                foreach (Match match in matches)
                {
                    //loggingService.Warning("DLOpenerRegex string size pre match" + modified.Length);
                    modified = modified.Replace(match.Value, "");
                    //loggingService.Warning("DLOpenerRegex string size post match" + modified.Length);
                }
            }

            {
                var matches = DLCloserRegex.Matches(modified);

                foreach (Match match in matches)
                {
                    //loggingService.Warning("DLCloserRegex string size pre match" + modified.Length);
                    modified = modified.Replace(match.Value, "");
                    //loggingService.Warning("DLCloserRegex string size post match" + modified.Length);
                }
            }

            {
                var matches = DTOpenerRegex.Matches(modified);

                foreach (Match match in matches)
                {
                    //loggingService.Warning("DTOpenerRegex string size pre match" + modified.Length);
                    modified = modified.Replace(match.Value, "");
                    //loggingService.Warning("DTOpenerRegex string size post match" + modified.Length);
                }
            }

            {
                var matches = DTCloserRegex.Matches(modified);

                foreach (Match match in matches)
                {
                    //loggingService.Warning("DTCloserRegex string size pre match" + modified.Length);
                    modified = modified.Replace(match.Value, "");
                    //loggingService.Warning("DTCloserRegex string size post match" + modified.Length);
                }
            }

            {
                var matches = CSSBlockRegex.Matches(modified);
                foreach (Match match in matches)
                {
                    modified = modified.Replace(match.Value, "");
                }
            }

            modified = modified.Replace("<br style=\"clear: both\">", "");
            modified = modified.Replace("</div>","");
            modified = modified.Replace("<div id=\"attachment_11\" style=\"width: 310px\" class=\"wp - caption alignright\">","");

            if (modified.Length != input.Length)
            {
                loggingService.Warning("Post gallery cull the lengths are input : " + input.Length +
                    " modified : " + modified.Length + " and the difference is " + (input.Length - modified.Length));
            }
            return modified;
        }

        //<style type='text\/css'>.*?<\/style>
        public static readonly Regex CSSBlockRegex = new Regex("<style type='text\\/css'>.*?<\\/style>", RegexOptions.Compiled | RegexOptions.Singleline);
        //<div\s*?id='gallery.*?><dl class='gallery-item'>
        public static readonly Regex GalleryDivOpenerRegex = new Regex("<div\\s*?id='gallery.*?><dl class='gallery-item'>", RegexOptions.Compiled | RegexOptions.Multiline);
        //<dl\s*?.*?>
        public static readonly Regex DLOpenerRegex = new Regex("<dl\\s*?.*?>", RegexOptions.Compiled | RegexOptions.Multiline);
        //</dl>
        public static readonly Regex DLCloserRegex = new Regex("</dl>", RegexOptions.Compiled | RegexOptions.Multiline);
        //<dt\s*?.*?>
        public static readonly Regex DTOpenerRegex = new Regex("<dt\\s*?.*?>", RegexOptions.Compiled | RegexOptions.Multiline);
        //</dt>
        public static readonly Regex DTCloserRegex = new Regex("</dt>", RegexOptions.Compiled | RegexOptions.Multiline);
    }
    /// <summary>
    /// TODO Fix up links to go from absolute to relative.
    /// </summary>
    [Export]
    public class LevelismBlogMarkdownConverter
    {
        [ImportingConstructor]
        public LevelismBlogMarkdownConverter(ILoggingService loggingService, Configuration configuration)
        {
            _loggingService = loggingService;
            _configuration = configuration;
        }

        public void Convert(string websiteUrl, string outputPath)
        {
            DownloadPageContents(websiteUrl);
            foreach (var post in _posts)
            {
                var outputPathFile = Path.Combine( outputPath , post.Title + ".md");
                File.WriteAllText(outputPathFile, post.Content);
            }
        }

        private string InsertFrontMatterMetaData(string input, PostMetaDataModel metaData)
        {
            var builder = new StringBuilder(input);

            builder = builder
                .Insert(
                0, 
                "---\n" + 
                JsonConvert.SerializeObject(metaData)
                .Replace(",",",\n")
                .Replace("{", "{\n")
                .Replace("}", "\n}") 
                + "\n---\n");

            return builder.ToString();
        }

        public void DownloadBlog(string websiteUrl, string outputPath, int maxPages = -1)
        {
            _loggingService.Info("downloading from url " + websiteUrl + " to " + outputPath);

            var startingUrlPage = 1;
            string convertedUrl() => @"http://levelism.com/page/" + startingUrlPage + "/";

            string SanitizePath(string path)
            {
                var newPath = path
                    .Replace("|", "_")
                    .Replace("/", "_")
                    .Replace(":", "")
                    .Replace(",", "")
                    .Replace(' ', '_');
                
                return System.Net.WebUtility.HtmlDecode(newPath);
            }

            while (DownloadPageContents(convertedUrl()))
            {
                startingUrlPage++;
                if (maxPages != -1 && startingUrlPage >= maxPages)
                {
                    break;
                }
            }
            foreach (var post in _posts)
            {
                var outputPathFile = 
                    Path.Combine(outputPath,post.CreatedSimple + "_" + SanitizePath(post.Title) + ".md");
     
                File.WriteAllText(outputPathFile, post.Content);
            }

        }

        private bool DownloadPageContents(string url)
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    var postContent = wc.DownloadString(url);
                    _posts.AddRange(ConvertHtmlFragmentToPostModel(postContent));
                }
                return true;
            }
            // aren't I relying on this throwing to exit the loop? Should I check for a 404 and not emit a warning 
            // in that case? Otherwise it's a bit misleading
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = ex.Response as HttpWebResponse;
                    if (response != null && response.StatusCode == HttpStatusCode.NotFound)
                    {
                        _loggingService.Info("Found the end of the blog. Everything is working nicely :)");
                    }
                    else
                    {
                        _loggingService.Warning(ex);
                    }
                }
                else
                {
                    _loggingService.Warning(ex);
                }
            }
            catch (Exception ex)
            {
                _loggingService.Warning(ex);
            }
            return false;
        }

        private IEnumerable<PostModel> ConvertHtmlFragmentToPostModel(string html)
        {
            var output = new List<PostModel>();
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);

            var postHtmlFragments = doc
                .DocumentNode
                .Descendants("div")
                .Where(x => x.Attributes.Where(y => y.Value == "levelism_content").SingleOrDefault() != null);

            foreach (var post in postHtmlFragments)
            {
                var allPElements = post.Descendants("p");
                var allDivElements = post.Descendants("div");

                var title = allPElements
                    .Where(x => x.Attributes.Select(y => y.Value).Contains("levelism_content_header_title"))
                    .Single();
                var date = allPElements
                    .Where(x => x.Attributes.Select(y => y.Value).Contains("levelism_content_header_date"))
                    .Single();
                var body = allDivElements
                    .Where(x => x.Attributes.Select(y => y.Value).Contains("levelism_content_body"))
                    .Single();
                var tagsRawText = allPElements
                    .Where(x => x.Attributes.Select(y => y.Value).Contains("levelism_content_footer_tags"))
                    .Single()
                    .InnerHtml;

                var imageResourceFolder = _configuration.ImageResourcesPath;
                var tagMatches = LinkRegex.Matches(tagsRawText);
                var tags = new List<string>();

                foreach(Match tagMatch in tagMatches)
                {
                    if (tagMatch.Success)
                    {
                        var groups = tagMatch.Groups;
                        var tag = groups["linktext"].Value;
                        tags.Add(ConvertHTMLToMarkdown(tag, imageResourceFolder));
                    }
                }

                var sanitizedBody = ConvertHTMLToMarkdown(body.InnerHtml, imageResourceFolder);

                var createdDate = DateTime.Parse(date.InnerText);
                var metaData = new PostMetaDataModel()
                {
                    Created = createdDate,
                    Title = ConvertHTMLToMarkdown(title.InnerText, imageResourceFolder),
                    Tags = new List<string>(tags)
                };

                sanitizedBody = InsertFrontMatterMetaData(sanitizedBody, metaData);

                var blogPost = new PostModel(sanitizedBody, metaData) { };
                output.Add(blogPost);
                _pagesDownloaded++;
                _loggingService.Info("Post downloaded. Total posts downloaded : " + _pagesDownloaded);

            }
            return output;
        }

        /// <summary>
        /// Takes in a html fragment and converts it to markdown including getting any image links, downloading 
        /// them locally and fixing up the image links.
        /// </summary>
        /// <param name="html"></param>
        /// <param name="imageFolderRoot"></param>
        /// <returns></returns>
        private string ConvertHTMLToMarkdown(string html,string imageFolderRoot)
        {

            bool HasAlreadyDownloadedLocalImage(string localUrl) => File.Exists(localUrl);
            /// Takes in a url link to an image and returns a unique relative path to save the image locally to. 
            string MakeUniqueLocalImagePathForURL(string url,string localImageFolderRoot)
            {
                _loggingService.Info("");
                _loggingService.Info("Working on " + url);
                // try aquiring images conforming to the year/month/image.png format so we can rebuild the folder structure for that
                var datedFilenameMatch = FolderDatedImageLinkRegex.Match(url);
                if (datedFilenameMatch.Success)
                {
                    _loggingService.Info("Found date formatted string");

                    var relativePath = datedFilenameMatch.Value.Substring(1).Replace('/', '\\');
                    // TODO we need to get resources/images as a relative path from config so I don't have to use magic strings like this
                    var relativeURL = "resources/images/" + relativePath.Replace('\\', '/');
                    // TODO not sure what I want here. see https://www.w3schools.com/html/html_filepaths.asp
                    //if (!relativeURL.StartsWith("/"))
                    //{
                    //    relativeURL.Insert(0, "/");
                    //}

                    //substring to remove front path separator and make path.combine stop failing
                    var localPath = Path.Combine(imageFolderRoot + "\\", relativePath); 

                    if (HasAlreadyDownloadedLocalImage(localPath))
                    {
                        _loggingService.Info("Local copy of image already exists, skipping download. Url is " + url);
                        return relativeURL;
                    }

                    try
                    {
                        _loggingService.Info("Local copy of image doesn't exist, downloading now. Url is " + url);

                        var localDirectoryPath = Path.GetDirectoryName(localPath);
                        Directory.CreateDirectory(localDirectoryPath);

                        using (WebClient wc = new WebClient())
                        {
                            if (!Path.HasExtension(localPath))
                            {
                                _loggingService.Error("Local path somehow doesn't have extension. The path is " + localPath);
                            }
                             wc.DownloadFile(url,localPath);
                            _loggingService.Info("Downloaded image successfully");
                        }
                            
                    }
                    catch(Exception ex )
                    {
                        _loggingService.Error(ex);
                        _loggingService.Error(ex.Message);
                        return null;
                    }
                    return relativeURL;
                }

                // otherwise put them in a folder using the format name_uniquehash to avoid collisiosn
                string urlFilename = url.Substring(url.LastIndexOf('/'));

                return null;

            }

            string removeExtraEmptyLines(string input)
            {
                List<string> newLines = new List<string>();
                var lines = input.Split('\n'); 
                bool shouldInsertWhitespace = false;

                foreach (var line in lines)
                {
                    if (string.IsNullOrWhiteSpace(line))
                    {
                        shouldInsertWhitespace = true;
                        continue;
                    }
                    if (shouldInsertWhitespace)
                    {
                        newLines.Add("\n\n");
                        shouldInsertWhitespace = false;
                    }
                    newLines.Add(line);
                }

                return string.Join("", newLines.ToArray());
            }

            string sanitizedString = html;
            sanitizedString = string.Join("", sanitizedString.ToCharArray().Where(x => ((int)x) < 127));
            sanitizedString = System.Net.WebUtility.HtmlDecode(sanitizedString);
            sanitizedString = WordpressGalleryConverter.Clean(sanitizedString, _loggingService);
            sanitizedString = sanitizedString.Replace("\t", "");
            sanitizedString = sanitizedString.Replace(@"<p>", "");
            sanitizedString = sanitizedString.Replace(@"</p>", "\n")
                //.Replace(@"</a>", "")
                .Replace(@"<strong>", "*")
                .Replace(@"</strong>", "*")
                .Replace(@"<br>", "")
                .Replace(@"<h1>", "# ")
                .Replace(@"</h1>", "")
                .Replace(@"<h2>", "## ")
                .Replace(@"</h2>", "")
                .Replace(@"<h3>", "### ")
                .Replace(@"</h3>", "")
                .Replace(@"</pre>", "\n```")
                .Replace(@"<ul>", "\n")
                .Replace(@"</ul>", "\n")
                .Replace(@"<li>", " - ")
                .Replace(@"</li>", "");

            var nonTrivialPTagMatches = PNonTrivialOpeningTagRegex.Matches(sanitizedString);
            foreach(Match match in nonTrivialPTagMatches)
            {
                if (match.Success)
                {
                    sanitizedString = sanitizedString.Replace(match.Value, "");
                }
            }

            var preMatches = PreOpeningBraceRegex.Matches(sanitizedString);
            foreach(Match match in preMatches)
            {
                if (match.Success)
                {
                    sanitizedString = sanitizedString.Replace(match.Value, "```");
                }
            }

            var imgNestedInLinkMatches = ImgNestedInLinkRegex.Matches(sanitizedString);

            foreach (Match match in imgNestedInLinkMatches)
            {
                // TODO convert the paths to relative and with forward slashes
                if (match.Success)
                {
                    var groups = match.Groups;
                    var link = groups["link"].Value;
                    var imageURL = groups["imagesource"].Value;
                    var localLinkURL = MakeUniqueLocalImagePathForURL(link, imageFolderRoot);
                    var localImageURL = MakeUniqueLocalImagePathForURL(imageURL, imageFolderRoot);

                    _loggingService.Info("");
                    _loggingService.Info("nested image link found");
                    _loggingService.Info("link : " + link);
                    _loggingService.Info("imageurl : " + imageURL);
                    _loggingService.Info("localLinkURL : " + localLinkURL);
                    _loggingService.Info("localImageURL : " + localImageURL);

                    // this will preserve the thumbnail/big photo releationship but my big ones are already so old and samll there's no point for thumbs
                    // var markdownFormattedLink = "[![" + localLinkURL + "](" + localImageURL + ")](" + localLinkURL + ")";

                    if (localLinkURL != null)
                    {
                        var markdownFormattedLink = "[![" + localLinkURL + "](" + localLinkURL + ")](" + localLinkURL + ")";
                        sanitizedString = sanitizedString.Replace(match.Value, markdownFormattedLink);
                    }
                    else
                    {
                        var markdownFormattedLink = "[![" + localImageURL + "](" + localImageURL + ")](" + localImageURL + ")";
                        sanitizedString = sanitizedString.Replace(match.Value, markdownFormattedLink);
                    }
                }
            }

            var imgMatches = ImgRegex.Matches(sanitizedString);
            // do image dowload/manipulation code here so we can dowenload and fix up the links
            foreach (Match match in imgMatches)
            {
                if (match.Success)
                {
                    var groups = match.Groups;
                    var imageLink = groups["link"].Value;
                    var localImageLink = MakeUniqueLocalImagePathForURL(imageLink, imageFolderRoot);
                    var markdownFormattedLink = "![" + localImageLink + "](" + localImageLink + ")";
                    sanitizedString = sanitizedString.Replace(match.Value, markdownFormattedLink);
                }
            }

            var linkMatches = LinkRegex.Matches(sanitizedString);

            foreach (Match match in linkMatches)
            {
                if (match.Success)
                {
                    var groups = match.Groups;
                    var imageLink = groups["link"].Value;
                    var markdownFormattedLink = "[" + imageLink + "](" + imageLink + ")";
                    sanitizedString = sanitizedString.Replace(match.Value, markdownFormattedLink);
                }

            }

            sanitizedString = removeExtraEmptyLines(sanitizedString);

            return sanitizedString.Trim();
        }

        private object _postsLock = new object();
        private int _pagesDownloaded;
        private readonly List<PostModel> _posts = new List<PostModel>();
        private readonly ILoggingService _loggingService;
        private readonly Configuration _configuration;

        // \/?\d{4}\/\d{2}\/\w+.\w+
        // V2
        // \/?\d{4}\/\d{2}\/.*\..*
        private readonly static Regex FolderDatedImageLinkRegex = new Regex("\\/?\\d{4}\\/\\d{2}\\/.*\\..*", RegexOptions.Compiled | RegexOptions.Multiline);

        // <a href="(?<link>.*?)"\s*(.*?)>(?<linktext>.*?)<\/a>
        // V2
        // <a\s.*?\shref="(?<link>.*?)"\s*(.*?)>\s(?<linktext>.*?)\s<\/a>
        // V3
        // <a\s*?.*?\s*?href="(?<link>.*?)"\s*?(.*?)>\s*?(?<linktext>.*?)\s*?<\/a>
        private readonly static Regex LinkRegex = new Regex("<a\\s*?.*?\\s*?href=\"(?<link>.*?)\"\\s*?(.*?)>\\s*?(?<linktext>.*?)\\s*?<\\/a>", RegexOptions.Compiled | RegexOptions.Multiline);

        // <img\s(.*?)src="(?<link>.*?)"\s(.*?)">
        private readonly static Regex ImgRegex = new Regex("<img\\s(.*?)src=\"(?<link>.*?)\"\\s(.*?)\">", RegexOptions.Compiled | RegexOptions.Multiline);

        // <a\s*href="(?<link>.*?)"(.*?)><img\s*(.*?)src="(?<imagesource>(.*?))"\s*(.*?)><\/a>
        // V2
        // <a\s*href=("|')(?<link>.*?)("|')(.*?)><img\s*(.*?)src="(?<imagesource>(.*?))"\s*(.*?)><\/a>
        private readonly static Regex ImgNestedInLinkRegex = new Regex("<a\\s*href=(\"|')(?<link>.*?)(\"|')(.*?)><img\\s*(.*?)src=\"(?<imagesource>(.*?))\"\\s*(.*?)><\\/a>", RegexOptions.Compiled | RegexOptions.Multiline);

        // <pre(.*?)>
        private readonly static Regex PreOpeningBraceRegex = new Regex("<pre(.*?)>", RegexOptions.Compiled | RegexOptions.Multiline);

        // <p\s.*>
        private readonly static Regex PNonTrivialOpeningTagRegex = new Regex("<p\\s.*?>", RegexOptions.Compiled | RegexOptions.Multiline);
    }
}
