﻿namespace StaticRazor.Model
{
    public class AboutModel
    {
        public AboutModel(string title, string content)
        {
            Title = title;
            Content = content;
        }

        public string Title { get; }
        public string Content { get; }
    }
}