﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticRazor.Model
{
    public enum LevelDesignReleaseStatus
    {
        Unreleased,
        Unfinished,
        Released
    }

    public class LevelDesignModelMetaData
    {
        public string LevelName { get; set; }
        public GameModelReleaseStatus ReleaseStatus { get; set; }
        public string ReleaseDate { get; set; }
        public string ReleaseURL { get; set; }
        public string DevelopmentTime { get; set; }
        public string Category { get; set; }
        public string ImagesPath { get; set; }
        public IEnumerable<string> Images { get; set; }
        public string ImageHeaderName { get; set; }
        public string MasterPageImage { get; set; }
    }

    public class LevelDesignModel
    {
        public LevelDesignModel(string content, LevelDesignModelMetaData metaData, string fileName, Configuration config)
        {
            Content = content;
            MetaData = metaData;
            FileName = fileName;
            _config = config;
            _stats = new Lazy<Dictionary<string, string>>(() => ModelStatics.GetModelStats(this));

            _images = MetaData.Images != null ?
                MetaData.Images.Select(x => _config.ImageResourcesURL + "/" + "level_design" + "/" + MetaData.ImagesPath + "/" + x) : _images;
        }

        [ModelStat]
        public string LevelName => MetaData.LevelName;
        [ModelStat]
        public string ReleaseStatus => MetaData.ReleaseStatus.ToString();
        [ModelStat]
        public string ReleaseDate => MetaData.ReleaseDate;
        [ModelStat]
        public string ReleaseURL => MetaData.ReleaseURL;
        [ModelStat]
        public string DevelopmentTime => MetaData.DevelopmentTime;
        [ModelStat]
        public string Category => MetaData.Category;

        public string ImageHeaderName => _config.ImageResourcesURL + "/" + "level_design" + "/" + MetaData.ImagesPath + "/" + MetaData.ImageHeaderName;
        public string MasterPageImage => "resources/images/level_design/master_page_images/" + MetaData.MasterPageImage;

        public string FileName { get; }
        public string FileNameHTML => "level_design_" + Path.GetFileNameWithoutExtension(FileName) + ".html";
        public IEnumerable<string> Images => _images;
        public Dictionary<string, string> Stats => _stats.Value;
        public LevelDesignModelMetaData MetaData { get; set; }
        public string Content { get; set; }

        private Lazy<Dictionary<string, string>> _stats;
        private readonly IEnumerable<string> _images = new List<string>();
        private Configuration _config;
    }
}