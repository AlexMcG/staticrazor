﻿using System.Collections.Generic;

namespace StaticRazor.Model
{
    public class ProgrammingProjectMetaData
    {
        public string Title { get; set; }
        public string ReleaseDate { get; set; }
        public List<string> ProgrammingLanguages { get; set; }
        public List<string> Libraries { get; set; }
        public List<string> PublicRepositories { get; set; }
    }

    public class ProgrammingProjectModel
    {
        public ProgrammingProjectModel(ProgrammingProjectMetaData metaData, string content, Configuration config)
        {
            MetaData = metaData;
            Content = content;
            ProgrammingLanguages = MetaData.ProgrammingLanguages != null ? MetaData.ProgrammingLanguages : new List<string>();
            Libraries = MetaData.Libraries != null ? MetaData.Libraries : new List<string>();
            PublicRepositories = MetaData.PublicRepositories != null ? MetaData.PublicRepositories : new List<string>();
        }
        public string Content { get; set; }
        public IEnumerable<string> ProgrammingLanguages { get; }
        public IEnumerable<string> Libraries { get; }
        public IEnumerable<string> PublicRepositories { get; }
        public ProgrammingProjectMetaData MetaData { get; set; }
    }
}
