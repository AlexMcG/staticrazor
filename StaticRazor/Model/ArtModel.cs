﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticRazor.Model
{
    public class ArtModelMetaData
    {
        public string Title { get; set; }
        public string ReleaseDate { get; set; }
        public string ImagesPath { get; set; }
        public List<string> Images { get; set; }
    }

    public class ArtModel
    {
        public ArtModel(ArtModelMetaData metaData,string content, Configuration config)
        {
            MetaData = metaData;
            Content = content;
            Images = MetaData.Images != null ?
                MetaData.Images.Select(x => config.ImageResourcesURL + "/" + "art" + "/" + MetaData.ImagesPath + "/" + x) : new List<string>();
        }
        public IEnumerable<string> Images { get; }
        public ArtModelMetaData MetaData { get; set; }
        public string Content { get; set; }
    }

    // dont need anything special so the model is also the viewmodel
    //public class ArtViewModel
    //{
    //    //public string Title { get; set; }
    //    //public DateTime ReleaseDate { get; set; }
    //    //public string ImagesPath { get; set; }
    //    //public List<string> Images { get; set; }
    //}

}
