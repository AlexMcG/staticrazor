﻿using System.Collections.Generic;
using System.Linq;

namespace StaticRazor.Model
{
    public static class ModelStatics
    {
        public static Dictionary<string,string> GetModelStats<T>(T model)
        {
            return typeof(T).GetProperties()
             .Where(x => x.CustomAttributes.Any(y => y.AttributeType == typeof(ModelStatAttribute)))
             .Select(x => new
             {
                 Key = x.Name,
                 Value = x.GetValue(model)?.ToString()
             })
             .Where(x => !string.IsNullOrEmpty(x.Value))
             .ToDictionary(x => x.Key, x => x.Value);
        }
    }
}