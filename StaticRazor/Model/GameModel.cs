﻿using StaticRazor.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace StaticRazor.Model
{
    public enum GameModelReleaseStatus
    {
        Unreleased,
        Released
    }

    public class GameModelMetaData
    {
        public IEnumerable<string> Platforms { get; set; }
        public string GameName { get; set; }
        public GameModelReleaseStatus ReleaseStatus { get; set; }
        public string ReleaseDate { get; set; }
        public string ReleaseURL { get; set; }
        public string DevelopmentTime { get; set; }
        public IEnumerable<string> ProgrammingLanguages { get; set; }
        public string Engine { get; set; }
        public IEnumerable<string> LibrariesUsed { get; set; }
        public string Category { get; set; }
        public string ImageHeaderName { get; set;  }
        public IEnumerable<string> Team { get; set; }
        public IEnumerable<string> Images { get; set; }
        public IEnumerable<string> YouTubeVideos { get; set; }
        public string ImagesPath { get; set; }
        public string ImagePageHeaderImage { get; set; }
    }

    public class ModelStatAttribute : Attribute { }

    /// <summary>
    /// TODO : This is almost a viewmodel but it still holds the pages content. Need to pull that out.
    /// </summary>
    public class GameModel
    {
        public GameModel(string content, GameModelMetaData metaData, string fileName, Configuration config,YouTubeService youTubeService)
        {
            Content = content;
            MetaData = metaData;
            FileName = fileName;
            _config = config;
            _youTubeService = youTubeService;

            // Stats contains a kvp for every property in this class that has the GameModelStat attribute and has a non-null value.
            // The key is the name of the property and the value is the properties value.
            // We do this lazily since iterating properties requires a fully constructed class instance.
            _stats = new Lazy<Dictionary<string, string>>(() => ModelStatics.GetModelStats(this));

            Images = MetaData.Images != null ? 
                MetaData.Images.Select(x => _config.ImageResourcesURL + "/" + "Games" + "/" + MetaData.ImagesPath + "/" + x) : new List<string>();

            YouTubeVideos = metaData.YouTubeVideos != null ?
                metaData.YouTubeVideos.Select(x => _youTubeService.ConvertYoutubeURLToEmbedCode(x)) : new List<string>();
        }

        [ModelStat]
        public string Platforms => MetaData.Platforms == null ? null : string.Join(" ", MetaData.Platforms);
        [ModelStat]
        public string GameName => MetaData.GameName;
        [ModelStat]
        public string ReleaseStatus => MetaData.ReleaseStatus.ToString();
        [ModelStat]
        public string ReleaseDate => MetaData.ReleaseDate;
        [ModelStat]
        public string ReleaseURL => MetaData.ReleaseURL;
        [ModelStat]
        public string DevelopmentTime => MetaData.DevelopmentTime;
        [ModelStat]
        public string ProgrammingLanguages => MetaData.ProgrammingLanguages == null ? null : string.Join(" ", MetaData.ProgrammingLanguages);
        [ModelStat]
        public string Engine => MetaData.Engine;
        [ModelStat]
        public string LibrariesUsed => MetaData.LibrariesUsed == null ? null : string.Join(" ", MetaData.LibrariesUsed);
        [ModelStat]
        public string Category => MetaData.Category;
        [ModelStat]
        public string Team => MetaData.Team == null ? null : string.Join(", ", MetaData.Team);

        /// <summary>
        /// crap this is a stupid name. I need to learn how to use the gnu commandline tools better so I can rename this across code/data.
        /// </summary>
        public string ImagePageHeaderImage => "resources/images/Games/" + MetaData.ImagePageHeaderImage;

        public string ImageHeaderName => "resources/images/Games/" + MetaData.ImageHeaderName;
        public string FileName { get; }
        public string FileNameHTML => "game_page_" + Path.GetFileNameWithoutExtension(FileName) + ".html";
        public IEnumerable<string> Images { get; }
        public IEnumerable<string> YouTubeVideos { get; }
        public Dictionary<string, string> Stats => _stats.Value;
        public GameModelMetaData MetaData { get; set; }
        public string Content { get; set; }

        private Lazy<Dictionary<string, string>> _stats;
        private Configuration _config;
        private readonly YouTubeService _youTubeService;
    }
}