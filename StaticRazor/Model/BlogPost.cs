﻿using System;
using System.Collections.Generic;
using System.IO;

namespace StaticRazor.Model
{
    public class PostMetaDataModel
    {
        public DateTime Created { get; set; } = DateTime.MinValue;
        public string Title { get; set; } = "Untitled";
        public List<string> Tags { get; set; } = new List<string>();
    }

    public class PostModel
    {
        public PostModel(string content, PostMetaDataModel metaData,string fileName = null)
        {
            Content = content;
            MetaData = metaData;
            FileName = fileName;
        }

        public string Title => MetaData.Title;
        public string Content { get; }
        public string Created => MetaData.Created.ToString("g");
        public string CreatedSimple => MetaData.Created.ToString("yyyy-MM-dd");
        // the next 2 properties should be in the viewmodel since it gets the configuration injected. the path can
        // be determined at that point. While we're at it, remove the filename param from the constructor for the model.
        public string FileName { get;  }
        public string RelativeLink => Path.GetFileNameWithoutExtension(FileName) + ".html";

        public PostMetaDataModel MetaData { get; }
    }
}