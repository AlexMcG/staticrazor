﻿using System;
using System.Collections.Generic;
using System.Composition;
using System.IO;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace StaticRazor
{
    static class FileSystemUtil
    {
        /// <summary>
        /// TODO move out into util class
        /// </summary>
        /// <param name="source"></param>
        /// <param name="dest"></param>
        public static void DirectoryCopyRecurisive(string source, string dest)
        {
            //Now Create all of the directories
            foreach (string dirPath in Directory.GetDirectories(source, "*",
                SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(source, dest));

            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(source, "*.*",
                SearchOption.AllDirectories))
                File.Copy(newPath, newPath.Replace(source, dest), true);
        }

        /// <summary>
        /// Takes in a string and returns a new string which if used as a filename would be valid. 
        /// Does not require an extension for the input string.
        /// 
        /// EX Super|Duper => SuperDuper
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string MakeValidFileNameString(string input)
        {
            var sanitizedInput = input
             .Replace("|", "_")
             .Replace("/", "_")
             .Replace(":", "")
             .Replace(",", "")
             .Replace(' ', '_');

            return System.Net.WebUtility.HtmlDecode(sanitizedInput);
        }
    }

    public class ProjectFileWatcher
    {
        public ProjectFileWatcher(params string[] projectPaths)
        {
            _fileChangedEvent = new Subject<ProjectFileWatcher>();

            foreach (var projectPath in projectPaths)
            {
                var fileSystemWatcher = new FileSystemWatcher(projectPath);
                fileSystemWatcher.IncludeSubdirectories = true;
                fileSystemWatcher.NotifyFilter = NotifyFilters.LastWrite;
                fileSystemWatcher.Changed += OnFileChanged;
                fileSystemWatcher.EnableRaisingEvents = true;
                _fileSystemWatchers.Add(fileSystemWatcher);
            }
        }

        public IObservable<ProjectFileWatcher> FileChanged => _fileChangedEvent.AsObservable();

        private void OnFileChanged(object sender, FileSystemEventArgs e)
        {
            _fileChangedEvent.OnNext(this);
        }

        private readonly List<FileSystemWatcher> _fileSystemWatchers = new List<FileSystemWatcher>();
        private readonly Subject<ProjectFileWatcher> _fileChangedEvent;
    }
}