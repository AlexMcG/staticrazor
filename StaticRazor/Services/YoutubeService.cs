﻿using System.Composition;
using System.Text;
using System.Text.RegularExpressions;

namespace StaticRazor.Services
{
    [Export]
    public class YouTubeService
    {
        [ImportingConstructor]
        public YouTubeService(ILoggingService loggingService, Configuration config)
        {
            _loggingService = loggingService;
            _config = config;
        }

        public string ConvertYoutubeURLToEmbedCode(string url)
        {
            Match urlMatch = _youtubeRegex.Match(url);
            if (urlMatch != null)
            {
                string modifiedUrl = 
                    "https://www.youtube.com/embed/" 
                    + urlMatch.Groups["youtubeHash"].Value 
                    + "?feature=oembed";
                return string.Format(_regexReplacementTemplate, modifiedUrl);
            }
            else
            {
                _loggingService.Error("unable to parse youtube url");
            }

            return "(youtube service error)";
        }

        public string FindAndConvertYoutubeLinksToEmbedTags(string content)
        {
            var builder = new StringBuilder(content);
            foreach(Match match in _youtubeRegex.Matches(content))
            {
                builder.Replace(match.Value, ConvertYoutubeURLToEmbedCode(match.Value));
            }

            return builder.ToString();
        }

        private readonly ILoggingService _loggingService;
        private readonly Configuration _config;

        private static readonly Regex _youtubeRegex = new Regex(@"https:\/\/www\.youtube\.com\/watch\?v=(?<youtubeHash>.*)", RegexOptions.Compiled);
        private const string _regexReplacementTemplate = "<iframe width=\"100%\" height=\"562.5\" src=\"{0}\" frameborder=\"0\" allowfullscreen=\"\" max-width=\"100%\" max-height=\"562.5\"></iframe>";
    }
}