﻿using NLog;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.Composition;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Tasks;

namespace StaticRazor.Services
{
    public interface ILoggingService
    {
        void Error(string message);
        void Error(Exception ex, string message = "");
        void Warning(string message);
        void Warning(Exception ex, string message = "");
        void Info(string message);
        IObservable<string> LogEvent { get; }
    }

    [Export(typeof (ILoggingService)), Shared]
    public class LoggingService : ILoggingService
    {
        [ImportingConstructor]
        public LoggingService(Configuration config)
        {
            _configuration = config;
            var logConfig = new NLog.Config.LoggingConfiguration();
            ConsoleTarget target = new ConsoleTarget("butt") { Layout = @"${date:format=HH\:mm\:ss} ${level} ${message} ${exception:format=ToString,StackTrace}" };
            logConfig.AddTarget(target);
            logConfig.AddRuleForAllLevels(target); // all to console

            _memoryTarget = new MemoryTarget("memTarget") { Layout = @"${date:format=HH\:mm\:ss.fff} ${level} ${message} ${exception:format=ToString,StackTrace}" };
            logConfig.AddTarget(_memoryTarget);
            logConfig.AddRuleForAllLevels(_memoryTarget); // all to console

            LogManager.Configuration = logConfig;
           _logger = LogManager.GetLogger(_configuration.LoggerName);

            _loggerSubject = new Subject<string>();
            Task.Run(() =>
            {
                int currentLogIdx = 0;
                while (true)
                {
                    if (currentLogIdx < _memoryTarget.Logs.Count - 1)
                    {
                        currentLogIdx++;
                        _loggerSubject.OnNext(_memoryTarget.Logs[currentLogIdx]);
                    }
                    Thread.Sleep(100);
                }
            });
        }

        public IObservable<string> LogEvent => _loggerSubject.AsObservable();

        private void PushLogEvents()
        {
            //lock (_pushLogEventsLock)
            //{
            //    //if (_isPushingEvents)
            //    //{
            //    //    return;
            //    //}
            //    //_isPushingEvents = true;

            //    var logs = new List<string>(_memoryTarget.Logs);
            //    _memoryTarget.Logs.Clear();
            //    foreach (var log in logs)
            //    {
            //        _loggerSubject.OnNext(log);
            //    }
            //    //_isPushingEvents = false;
            //}
        }

        public void Error(string message)
        {
            _logger.Error(message);
            PushLogEvents();
        }

        public void Error( Exception ex, string message = "")
        {
            _logger.Error(ex, message);
            PushLogEvents();
        }

        public void Warning(string message)
        {
            _logger.Warn(message);
            PushLogEvents();
        }

        public void Warning(Exception ex, string message = "")
        {
            _logger.Warn(ex, message);
            PushLogEvents();
        }

        public void Info(string message)
        {
            _logger.Info(message);
            PushLogEvents();
        }

        private Configuration _configuration;
        private Logger _logger;
        private MemoryTarget _memoryTarget;
        private readonly Subject<string> _loggerSubject;

        private object _pushLogEventsLock = new object();
        //private bool _isPushingEvents = false;
    }
}