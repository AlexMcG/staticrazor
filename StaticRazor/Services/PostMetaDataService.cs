﻿using Newtonsoft.Json;
using StaticRazor.Model;
using System;
using System.Composition;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

[assembly: InternalsVisibleTo("StaticRazorTests")]
namespace StaticRazor.Services
{
    /// <summary>
    /// TODO : Make this generic
    /// </summary>
    [Export]
    public class MetaDataParsingService
    {
        [ImportingConstructor]
        public MetaDataParsingService(ILoggingService loggingService, Configuration config)
        {
            _loggingService = loggingService;
            _config = config;
        }

        public MetaDataParsingService()
        {
            
        }

        public T ParseMetaData<T>(string metaDataJsonFragment)
        {
            T returnValue = default(T);
            try
            {
                if (string.IsNullOrEmpty(metaDataJsonFragment))
                {
                    //error
                }
                returnValue = JsonConvert.DeserializeObject<T>(metaDataJsonFragment);
            }
            catch (Exception ex)
            {
                _loggingService.Error(ex);
                _loggingService.Error("Meta data json fragment being deserialized when exception was thrown : " + metaDataJsonFragment);
            }
            return returnValue;
        }

        public string ExtractMetaDataJsonFragment(string post)
        {
            string postMetaData = "";

            try
            {
                if (!post.StartsWith("---"))
                {
                    throw new FormatException("First 3 characters of the document are not " +
                        "the --- frontmatter opening dashes.");
                }
                var postMetaDataMatch = FrontMatterRegex.Match(post);
                if (!postMetaDataMatch.Success)
                {
                    throw new FormatException("Unable to parse post metadata fragement from " +
                        " document. Possibly the front matter header is malformed.");
                }

                postMetaData = postMetaDataMatch.Groups[2].Value;
                if (string.IsNullOrEmpty(postMetaData))
                {
                    throw new FormatException("Post meta data successfully parsed but it is empty.");
                }
            }
            catch (Exception ex)
            {
                _loggingService.Warning("Unable to parse metadata");
                _loggingService.Warning(ex);
                return string.Empty;
            }
            return postMetaData.Trim();
        }

        private readonly ILoggingService _loggingService;
        private readonly Configuration _config;
        private const string FrontMatterRegexString = @"(---)(.*)(---)";
        private static readonly Regex FrontMatterRegex = 
            new Regex(FrontMatterRegexString, RegexOptions.Compiled | RegexOptions.Singleline);
    }
}
