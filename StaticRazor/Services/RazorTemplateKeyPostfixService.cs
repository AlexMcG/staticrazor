﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticRazor.Services
{
    /// <summary>
    /// If I'm editing templates while working on my website I need to use a different
    /// key for the file I'm editing or else Razor will throw and say I'm using the same
    /// key on a different template.
    /// 
    /// So this will store a random number I'll append to the template string and will increment when 
    /// a template is updated.
    /// </summary>
    class RazorTemplateKeyPostfixService
    {
        /// <summary>
        /// Dumb implementation. This will force a rebuild of the template even if it hasn't been modified.
        /// </summary>
        /// <param name="keyPrefix"></param>
        /// <returns></returns>
        //public static string GetTemplateKey(string keyPrefix) => keyPrefix + _random.Next();

        // Hack because I don't want to deal with this stuff right now.
        public static string GetTemplateKey(string keyPrefix) => Guid.NewGuid().ToString();

        private static Random _random = new Random();
    }
}
