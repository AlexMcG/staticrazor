﻿using System;
using System.Collections.Generic;
using System.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StaticRazor.Services
{
    /// <summary>
    /// 
    /// Color template values (in order)
    /// 
    /// theme_color_01
    /// theme_color_02
    /// theme_color_03
    /// theme_color_04
    /// theme_color_05
    /// 
    /// </summary>
    [Export]
    public class ColorTemplateApplicator
    {
        [ImportingConstructor]
        public ColorTemplateApplicator(ILoggingService loggingService)
        {
            _loggingService = loggingService;
        }

        public void ApplyTemplate(string pathToTemplate,string pathToCssSource,string pathToCssTarget)
        {
            var cssFiles = Directory.GetFiles(pathToCssSource,"*",SearchOption.AllDirectories).Where(x => x.EndsWith("css"));
            IEnumerable<string> colors = GetTemplateColors(pathToTemplate);

            var templateDict = _orderedTemplateKeys
                .Zip(colors,(k,v) => new { k, v })
                .ToDictionary(x => x.k,x => x.v);

            _loggingService.Info("Applying templates");

            foreach (var cssFile in cssFiles)
            {
                int attempts = 0;
                while (attempts < 10)
                {
                    try
                    {
                        var stringBuilder = new StringBuilder(File.ReadAllText(cssFile));
                        foreach (var templatePair in templateDict)
                        {
                            stringBuilder.Replace(templatePair.Key, "#" + templatePair.Value);
                        }
                        var outputPath = Path.Combine(pathToCssTarget, Path.GetFileName(cssFile));
                        File.WriteAllText(outputPath, stringBuilder.ToString());
                        break;
                    }
                    catch(Exception ex)
                    {
                        _loggingService.Warning(ex);
                        attempts++;
                        Thread.Sleep(100);
                    }
                }
                
            }
        }

        private List<string> GetTemplateColors(string pathToTemplate)
        {
            var templateColors = new List<string>();
            int attempts = 0;
            while (attempts < 10)
            {
                try
                {
                    templateColors = File.ReadAllLines(pathToTemplate).Select(x => x.Trim()).ToList();
                    break;
                }
                catch (Exception ex)
                {
                    _loggingService.Warning(ex);
                    attempts++;
                    Thread.Sleep(100);
                }
            }
            return templateColors;
        }

        private readonly ILoggingService _loggingService;

        private readonly List<string> _orderedTemplateKeys = new List<string>()
        {
            "[theme_color_01]",
            "[theme_color_02]",
            "[theme_color_03]",
            "[theme_color_04]",
            "[theme_color_05]",
        };
    }
}
