﻿using System;
using System.Composition;
using System.Text;
using System.Text.RegularExpressions;

namespace StaticRazor.Services
{
    [Export]
    public class MenuManipulator
    {
        [ImportingConstructor]
        public MenuManipulator(ILoggingService loggingService, Configuration config)
        {
            _loggingService = loggingService;
            _config = config;
        }

        public string GenerateMenuHTMLFragment(string menuHtmltemplate,string selectedKey, string unselectedCss,string selectedCSS)
        {
            StringBuilder menuBuilder = new StringBuilder(menuHtmltemplate);
            var matches = _keyRegex.Matches(menuHtmltemplate);

            foreach(Match match in matches)
            {
                if (match.Success)
                {
                    try
                    {
                        string matchGroupValue = match.Groups[0].Value;
                        string key = matchGroupValue.Trim(new char[] { '{', '}' });
                        if (key == selectedKey)
                        {
                            menuBuilder.Replace(matchGroupValue, selectedCSS);
                        }
                        else
                        {
                            menuBuilder.Replace(matchGroupValue, unselectedCss);
                        }
                    }
                    catch(Exception ex)
                    {
                        _loggingService.Error(ex);
                    }
                }
            }

            return menuBuilder.ToString();
        }

        private static readonly Regex _keyRegex = new Regex(@"{(.*?)}");
        private readonly ILoggingService _loggingService;
        private readonly Configuration _config;
    }
}