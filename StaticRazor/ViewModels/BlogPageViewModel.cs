﻿using StaticRazor.Model;
using StaticRazor.ViewModels;
using StaticRazor.WebsiteGeneration;
using System.Collections.Generic;

namespace StaticRazor.ViewModels
{
    public class BlogPageViewModel
    {
        public BlogPageViewModel(List<BlogPostViewModel> posts, int pageNumber)
        {
            Posts = posts;
            PageNumber = pageNumber;

            if (pageNumber != 0)
            {
                PreviousPageURL = GeneratePageURL(pageNumber - 1);
            }
            // TODO this will cause a 404 on the last page
            NextPageURL = GeneratePageURL(pageNumber + 1);
        }
        public List<BlogPostViewModel> Posts = new List<BlogPostViewModel>();
        public int PageNumber { get; }

        public string PreviousPageURL { get; }
        public string NextPageURL { get; }

        private string GeneratePageURL(int pageNumber) =>
            string.Format(WebsiteGenerator.BlogPageNameStringTemplate, pageNumber.ToString("0000"));
    }
}
