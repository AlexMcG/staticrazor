﻿using StaticRazor.Model;
using System.Collections.Generic;

namespace StaticRazor.ViewModels
{
    public class BlogPostViewModel
    {
        public BlogPostViewModel(Configuration config,PostModel model)
        {
            _config = config;
            _model = model;
        }

        public string Title => _model.Title;
        public string Content => _model.Content;
        public string Created => _model.Created;
        public string CreatedSimple => _model.CreatedSimple;
        public string FileName => _model.FileName;
        public string RelativeLink => _model.RelativeLink;
        public PostMetaDataModel MetaData => _model.MetaData;

        public List<TagViewModel> Tags { get; } = new List<TagViewModel>();

        private PostModel _model;
        private Configuration _config;
    }
}
