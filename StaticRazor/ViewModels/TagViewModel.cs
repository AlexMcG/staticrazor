﻿using System.Collections.Generic;
using System.Linq;

namespace StaticRazor.ViewModels
{
    public class TagViewModel
    {
        public TagViewModel(string tag, IEnumerable<BlogPostViewModel> posts)
        {
            Tag = tag;
            Posts = posts;
        }
        
        public string FileName => "tag_page_" + Tag + ".html";
        public string Tag { get; }
        public int NumberOfPostsWithTag => Posts.Count(); // might consider turning Posts into a list so count is O(1)
        public IEnumerable<BlogPostViewModel> Posts { get; }
        public IEnumerable<BlogPostViewModel> PostsOrderedByDate => Posts.OrderByDescending(x => x.Created);
    }
}