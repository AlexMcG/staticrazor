﻿using Markdig;
using StaticRazor.Model;
using StaticRazor.Services;
using StaticRazor.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace StaticRazor.WebsiteGeneration
{
    public class WebsiteViewModel
    {
        public WebsiteViewModel(Configuration config,
            MetaDataParsingService postMetaDataService,
            ILoggingService loggingService,
            YouTubeService youtubeService)
        {
            _config = config;
            _metaDataService = postMetaDataService;
            _loggingService = loggingService;

            var pipeline = new MarkdownPipelineBuilder().UseYamlFrontMatter().Build();

            var blogPostModels = Directory.GetFiles(_config.BlogPath)
                .Reverse()
                .Select(x => new { File = File.ReadAllText(x), FileName = x })
                .Select(x => new
                {
                    MetaData = _metaDataService
                        .ParseMetaData<PostMetaDataModel>(
                            _metaDataService.ExtractMetaDataJsonFragment(x.File)),
                    HtmlContent = Markdown.ToHtml(x.File, pipeline),
                    x.FileName
                })
                .Select(x => new PostModel(x.HtmlContent, x.MetaData, x.FileName));

            BlogPosts = blogPostModels
                .Select(x => new BlogPostViewModel(_config, x))
                .ToList();

            // split blog pages into page groups
            int PostsPerPage = _config.BlogPostsPerPage;
            var blogPostQueue = new List<BlogPostViewModel>(BlogPosts);

            while (blogPostQueue.Count > 0)
            {
                var postsToTake = Math.Min(blogPostQueue.Count, PostsPerPage);
                var currentPagePosts = blogPostQueue.Take(postsToTake);
                BlogPages.Add(
                    new BlogPageViewModel(currentPagePosts.ToList(), BlogPages.Count));
                blogPostQueue.RemoveRange(0, postsToTake);
            }

            // setup tag pages
            TagPages = new Dictionary<string, TagViewModel>();
            var allTags = blogPostModels
                .SelectMany(x => x.MetaData.Tags)
                .Distinct();

            foreach (var tag in allTags)
            {
                var allPostsWithTag = BlogPosts
                    .Where(x => x.MetaData.Tags.Contains(tag));

                var tagPage = new TagViewModel(tag, allPostsWithTag);
                TagPages.Add(tag, tagPage);

                foreach (var postWithTag in allPostsWithTag)
                {
                    postWithTag.Tags.Add(tagPage);
                }
            }

            // setup game pages
            try
            {
                GamePages = Directory.GetFiles(_config.GamesPath)
                    .Select(x => new { File = File.ReadAllText(x), FullPathToFile = x })
                    .Select(x => new
                    {
                        MetaData = _metaDataService
                            .ParseMetaData<GameModelMetaData>(_metaDataService.ExtractMetaDataJsonFragment(x.File)),
                        HtmlContent = Markdown.ToHtml(youtubeService.FindAndConvertYoutubeLinksToEmbedTags( x.File), pipeline),
                        FileName = Path.GetFileName(x.FullPathToFile)
                    })
                    .Select(x => new GameModel(x.HtmlContent, x.MetaData, x.FileName, _config, youtubeService));

                GamePagesGroupedByCategory = GamePages
                    .GroupBy(x => x.Category)
                    .OrderBy(x => x.Key)
                    .ToDictionary(x => x.Key, x => (IEnumerable<GameModel>)x);

                // Not everything has a release date so this code as it stands is invalid
                //GamePagesGroupedByDate = GamePages
                //    .GroupBy(x => x.ReleaseDate)
                //    .OrderBy(x => x.Key)
                //    .ToDictionary(x => x.Key, x => (IEnumerable<GameModel>)x);
            }
            catch (Exception ex)
            {
                _loggingService.Error(ex);
            }

            // setup level design pages
            try
            {
                LevelDesignPages = Directory.GetFiles(_config.LevelDesignPath)
                    .Select(x => new { File = File.ReadAllText(x), FullPathToFile = x })
                    .Select(x => new
                    {
                        MetaData = _metaDataService
                            .ParseMetaData<LevelDesignModelMetaData>(_metaDataService.ExtractMetaDataJsonFragment(x.File)),
                        HtmlContent = Markdown.ToHtml(x.File, pipeline),
                        FileName = Path.GetFileName(x.FullPathToFile)
                    })
                    .Select(x => new LevelDesignModel(x.HtmlContent, x.MetaData, x.FileName, _config));


                LevelDesignGroupedByCategory = LevelDesignPages
                    .GroupBy(x => x.Category)
                    .OrderByDescending(x => x.Key)
                    .ToDictionary(x => x.Key, x => (IEnumerable<LevelDesignModel>)x);
            }
            catch (Exception ex)
            {
                _loggingService.Error(ex);
            }

            // art projects
            try
            {
                ArtModels = Directory.GetFiles(_config.ArtPath)
                .Select(x => new { FileContents = File.ReadAllText(x), PathToFile = x })
                .Select(x => new
                {
                    MetaData = _metaDataService
                            .ParseMetaData<ArtModelMetaData>(_metaDataService.ExtractMetaDataJsonFragment(x.FileContents)),
                    HtmlContent = Markdown.ToHtml(x.FileContents, pipeline),
                    FileName = Path.GetFileName(x.PathToFile)
                })
                .Select(x => new ArtModel(x.MetaData, x.HtmlContent,_config))
                .Reverse()
                .ToList();
            }
            catch (Exception ex)
            {
                _loggingService.Error(ex);
            }

            // programming projects
            try
            {
                ProgrammingProjectModels = Directory.GetFiles(_config.ProgrammingProjectsPath)
                .Select(x => new { FileContents = File.ReadAllText(x), PathToFile = x })
                .Select(x => new
                {
                    MetaData = _metaDataService
                            .ParseMetaData<ProgrammingProjectMetaData>(_metaDataService.ExtractMetaDataJsonFragment(x.FileContents)),
                    HtmlContent = Markdown.ToHtml(x.FileContents, pipeline),
                    FileName = Path.GetFileName(x.PathToFile)
                })
                .Select(x => new ProgrammingProjectModel(x.MetaData, x.HtmlContent, _config))
                .Reverse()
                .ToList();
            }
            catch (Exception ex)
            {
                _loggingService.Error(ex);
            }

            // about page
            try
            {
                AboutPage = new AboutModel(
                    "about(change this to not be a magic string", 
                    Markdown.ToHtml(File.ReadAllText(Path.Combine(_config.AboutPath, "about.md"))));
            }
            catch (Exception ex)
            {
                _loggingService.Error(ex);
            }
        }

        public List<ArtModel> ArtModels { get; } = new List<ArtModel>();

        public List<ProgrammingProjectModel> ProgrammingProjectModels { get; } = new List<ProgrammingProjectModel>();

        public List<BlogPostViewModel> BlogPosts { get; }
        public List<BlogPageViewModel> BlogPages { get; } = new List<BlogPageViewModel>();

        public IEnumerable<GameModel> GamePages { get; }
        public Dictionary<string, IEnumerable<GameModel>> GamePagesGroupedByCategory { get; }
        public Dictionary<string, IEnumerable<GameModel>> GamePagesGroupedByDate { get; }

        public IEnumerable<LevelDesignModel> LevelDesignPages { get; }
        public Dictionary<string, IEnumerable<LevelDesignModel>> LevelDesignGroupedByCategory { get; }

        public Dictionary<string, TagViewModel> TagPages { get; }
        public IEnumerable<TagViewModel> TagsInAlphabeticalOrder =>
            TagPages.Values.OrderBy(x => x.Tag);
        public IEnumerable<TagViewModel> TagsOrderedByPostCount =>
            TagPages.Values.OrderByDescending(x => x.NumberOfPostsWithTag);

        public AboutModel AboutPage { get; }

        private readonly Configuration _config;
        private readonly MetaDataParsingService _metaDataService;
        private readonly ILoggingService _loggingService;
    }
}