﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticRazorCLI
{

    /// <summary>
    /// /// Example commandlines
    /// 
    /// --create --input F:\Websites\Levelism --output F:\Websites\Levelism\output --live
    /// --export_levelism_blog --input http://levelism.com/page/2/ --output f:\temp
    /// --apply_color_template --input F:\Websites\Levelism\colors\future_theme_02.txt --css_source F:\Websites\Levelism\templates\css_source\layouts --css_target F:\Websites\Levelism\templates\css\layouts
    /// </summary>
    [ArgumentGroupCertification("c,x,a", EArgumentGroupCondition.ExactlyOneUsed)]
    class CommandlineParsingTarget
    {
        [ValueArgument(typeof(string), 'c', "create", Description = "Creates a website.")]
        public string CreateCommand;

        [ValueArgument(typeof(string), 'x', "export_levelism_blog", Description = "Snakes the levelism website blog at the given url" +
            "and converts it to markdown. Also downloads and fixes up image links locally")]
        public string ExportLevelismBlogCommand;

        [ValueArgument(typeof(string), 'a', "apply_color_template", Description = "Creates a website when given an input folder")]
        public string ApplyColorTemplateCommand;

        [ValueArgument(typeof(string), 'l', "live_mode", Description = "Makes the process watch your folders and automatically runs when it detects files have been changed/added/removed")]
        public string LiveMode;

        [ValueArgument(typeof(string), 'i', "input", Description = "Input for commands that need it.")]
        public string Input;

        [ValueArgument(typeof(string), 'o', "output", Description = "Output for commands that need it.")]
        public string Output;

        [ValueArgument(typeof(string), 's', "css_source", Description = "CSS source directory.")]
        public string CSSSource;

        [ValueArgument(typeof(string), 't', "css_target", Description = "Transformed CSS output directory.")]
        public string CSSTarget;
    }
    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
