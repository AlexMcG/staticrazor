﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Composition.Hosting;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;

namespace RazorDevEnvironment
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class RazorDevMainWindow : Window
    {
        public static RazorDevApp RazorDevApp => (RazorDevApp)Application.Current;

        public string PathToConfig = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\" + "config.json";

        public RazorDevMainWindow()
        {
            InitializeComponent();
            List<Assembly> allAssemblies = new List<Assembly>();
            string path = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            foreach (string dll in Directory.GetFiles(path, "*.dll"))
            {
                allAssemblies.Add(Assembly.LoadFile(dll));
            }

            var container = new ContainerConfiguration()
                .WithAssemblies(allAssemblies)
                .WithAssemblies(new List<Assembly>() { Assembly.GetExecutingAssembly() })
                .CreateContainer();

            ViewModel = container.GetExport<ApplicationViewModel>();

            ViewModel.Config.AutoSerializePath = PathToConfig;

            if (File.Exists(PathToConfig))
            {
                ViewModel.Config.Deserialize(PathToConfig);
            }

            DataContext = ViewModel;
           // ViewModel.LogEntries.CollectionChanged += (object sender, NotifyCollectionChangedEventArgs e) => { ScrollViewer_Log.ScrollToBottom(); };

            TabItem item = new TabItem();
            ScrollViewer scroll = new ScrollViewer();
            ListView list = new ListView();

            list.ItemsSource = ViewModel.LogViewSource;

            item.Header = "FullLogs";

            scroll.Content = list;
            item.Content = scroll;

            TabControlWebsiteRunLogs.Items.Add(item);
            TabControlWebsiteRunLogs.SelectedItem = item;
        }

        public ApplicationViewModel ViewModel { get; }

        private void CheckBox_Checked_AutomaticSiteGeneration(object sender, RoutedEventArgs e) =>
            ViewModel.StartColorTemplateFileWatcher();

        private void CheckBox_Unchecked_AutomaticSiteGeneration(object sender, RoutedEventArgs e) =>
               ViewModel.StopColorTemplateFileWatcher();

        private void Button_Click_BrowseToWebsiteRoot(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            var result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                TextBox_WebsiteRoot.Text = dialog.SelectedPath;
                ViewModel.Config.Serialize(PathToConfig);
            }
        }

        private void Button_Click_OutputPath(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            if (!string.IsNullOrEmpty(ViewModel.Config.WebsiteRoot) && Directory.Exists(ViewModel.Config.WebsiteRoot))
            {
                dialog.SelectedPath = ViewModel.Config.WebsiteRoot;
            }
            var result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                TextBox_OutputPath.Text = dialog.SelectedPath;
                ViewModel.Config.Serialize(PathToConfig);
            }
        }

        private void Button_Click_SelectedColorTemplate(object sender, RoutedEventArgs e)
        {
            AltColorTemplateSelection();
            //System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            //if (!string.IsNullOrEmpty(ViewModel.Config.WebsiteRoot) && Directory.Exists(ViewModel.Config.WebsiteRoot))
            //{
            //    dialog.SelectedPath = ViewModel.Config.WebsiteRoot;
            //}
            //var result = dialog.ShowDialog();
            //if (result == System.Windows.Forms.DialogResult.OK)
            //{
            //    TextBox_SelectedColorTemplate.Text = dialog.SelectedPath;
            //}
        }

        private void Button_Click_LevelismSnake(object sender, RoutedEventArgs e)
        {
            ViewModel.DownloadLevelismBlog(ViewModel.Config.LevelismURL);
        }

        private void AltColorTemplateSelection()
        {
            using (System.Windows.Forms.OpenFileDialog openDialog = new System.Windows.Forms.OpenFileDialog())
            {
                if (!string.IsNullOrEmpty(ViewModel.Config.WebsiteRoot) && Directory.Exists(ViewModel.Config.WebsiteRoot))
                {
                    openDialog.InitialDirectory = ViewModel.Config.WebsiteRoot;
                }
                if (openDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    TextBox_SelectedColorTemplate.Text = openDialog.FileName;
                }

            }
        }

        private void Textbox_WebsiteRoot_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e) =>
            ViewModel.Config.WebsiteRoot = TextBox_WebsiteRoot.Text;

        private void TextBox_OutputPath_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e) =>
            ViewModel.Config.Output = TextBox_OutputPath.Text;

        private void TextBox_SelectedColorTemplate_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e) =>
            ViewModel.CurrentlySelectedColorTemplate = TextBox_SelectedColorTemplate.Text;

        private void Textbox_WebsiteURL_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e) =>
            ViewModel.Config.LevelismURL = TextBox_WebsiteURL.Text;

        private void Textbox_LevelismBlogPagesToSnake_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            try
            {
                ViewModel.Config.LevelismBlogPagesToSnake = int.Parse(TextBox_LevelismBlogPagesToSnake.Text);
            }
            catch
            {
                
            }
        }
            
        private void ItemsControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

        }

        private async void Button_Click_GenerateWebsite(object sender, RoutedEventArgs e)
        {
            ViewModel.MakeNewLogForNextWebsiteGenerationRun();
            TabItem item = new TabItem();
            ScrollViewer scroll = new ScrollViewer();
            ListView list = new ListView();

            list.ItemsSource = ViewModel.CurrentWebsiteRunLog;
            ViewModel.CurrentWebsiteRunLog.CollectionChanged += 
                (object collection, NotifyCollectionChangedEventArgs args) => { scroll.ScrollToBottom(); };

            item.Header = DateTime.Now.ToString("F");

            scroll.Content = list;
            item.Content = scroll;

            TabControlWebsiteRunLogs.Items.Add(item);
            TabControlWebsiteRunLogs.SelectedItem = item;
            await ViewModel.GenerateWebsite();
        }

        private void ComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            foreach(var added in e.AddedItems)
            {
                var item = added as ComboBoxItem;
                ViewModel.LogFilter = item.Content.ToString();
            }
        }
    }
}
