﻿using StaticRazor;
using StaticRazor.Services;
using StaticRazor.WebsiteGeneration;
using System;
using System.Composition;
using System.IO;
using System.Threading.Tasks;

namespace RazorDevEnvironment
{
    [Export]
    public class FileWatcher
    {
        [ImportingConstructor]        
        public FileWatcher(
            ILoggingService loggingService, 
            ColorTemplateApplicator colorApplicator, 
            WebsiteGenerator websiteGenerator,
            Configuration config)
        {
            _loggingService = loggingService;
            _colorApplicator = colorApplicator;
            _websiteGenerator = websiteGenerator;
            _config = config;
        }

        public void StartColorTemplateWatch(string pathToCssSource, string pathToTemplate)
        {
            if (_livePreviewEnabled)
            {
                _loggingService.Warning("StartColorTemplateWatch called when watch was already on."
                    + "Sounds like a logic/concurrency error.");
                return;
            }

            var finalPathToTemplate = string.IsNullOrEmpty(pathToTemplate) ? _config.DefaultColorTemplate : pathToTemplate;

            ProjectFileWatcher cssWatcher = new ProjectFileWatcher(pathToCssSource, Path.GetDirectoryName(finalPathToTemplate), _config.BlogPath,_config.TemplatePath);
            //ProjectFileWatcher blogWatcher = new ProjectFileWatcher(_config.BlogPath);
            _livePreviewEnabled = true;

            // TODO Somehow convert this to a coroutine
            Task.Run(() =>
                {
                    using (IDisposable projectSubscription = cssWatcher
                       .FileChanged
                       .Subscribe((x) =>
                       {
                           lock(_generatingWebsiteLock)
                           {
                               if (!_isGeneratingWebsite)
                               {
                                   _isGeneratingWebsite = true;
                                   Task.Run(() =>
                                   {
                                       //_colorApplicator.ApplyTemplate(pathToTemplate, pathToCssSource, pathToCssTarget);

                                       _websiteGenerator.GenerateWebsite(_config.Output);
                                       _loggingService.Info("Built new website at " + _config.Output);

                                   });
                                   _isGeneratingWebsite = false;
                               }
                           }
                       }))
                    {
                        while (_livePreviewEnabled == true) ;
                    }
                });

            //Task.Run(() =>
            //{
            //    using (IDisposable projectSubscription = blogWatcher
            //       .FileChanged
            //       .Subscribe((x) =>
            //       {
            //       }))
            //    {
            //        while (_livePreviewEnabled == true) ;
            //    }
            //});
        }

        public void StopColorTemplateWatch() => _livePreviewEnabled = false;

        private bool _livePreviewEnabled;
        private bool _isGeneratingWebsite;
        private object _generatingWebsiteLock = new object();
        private readonly ILoggingService _loggingService;
        private readonly ColorTemplateApplicator _colorApplicator;
        private readonly WebsiteGenerator _websiteGenerator;
        private readonly Configuration _config;
    }
}