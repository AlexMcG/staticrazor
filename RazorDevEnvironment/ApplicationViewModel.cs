﻿using StaticRazor;
using StaticRazor.Services;
using StaticRazor.WebsiteGeneration;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Composition;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace RazorDevEnvironment
{
    [Export]
    public class ApplicationViewModel : INotifyPropertyChanged
    {
        private IDisposable _logSubscription;
        [ImportingConstructor]
        public ApplicationViewModel(
            FileWatcher fileWatcher,
            Configuration configuration,
            ILoggingService loggingService,
            LevelismBlogMarkdownConverter levelismMarkdownConverter,
            WebsiteGenerator websiteGenerator)
        {
            _config = configuration;
            _website = websiteGenerator;
            _fileWatcher = fileWatcher;
            _loggingService = loggingService;
            _levelismConverter = levelismMarkdownConverter;
            LogViewSource = CollectionViewSource.GetDefaultView(LogEntries);
            _logSubscription = _loggingService.LogEvent.Subscribe(x =>
            {
                Application.Current.Dispatcher.BeginInvoke((Action)(() =>
                {
                    if (x != null)
                    {
                        LogEntries.Add(x);
                        if (CurrentWebsiteRunLog != null)
                        {
                            CurrentWebsiteRunLog.Add(x);
                        }
                    }
                })
            , System.Windows.Threading.DispatcherPriority.Normal);
            }); 
            LogViewSource.Filter += (x) => x.ToString().Contains(LogFilter);
        }

        public string LogFilter
        {
            get => _logFilter;
            set
            {
                _logFilter = value;
                LogViewSource.Refresh();
            }
        }

        public ICollectionView LogViewSource { get; }

        public Dictionary<string, ObservableCollection<string>> WebsiteRunLogs = new Dictionary<string, ObservableCollection<string>>();
        public ObservableCollection<string> CurrentWebsiteRunLog;

        public ObservableCollection<string> LogEntries { get; } = new ObservableCollection<string>();

        public string CurrentlySelectedColorTemplate
        {
            get
            {
                if (string.IsNullOrEmpty(_currentlySelectedColorTemplate))
                {
                    return _config.DefaultColorTemplate;
                }
                else return _currentlySelectedColorTemplate;
            }
            set => _currentlySelectedColorTemplate = value;
        }
        public Configuration Config => _config;

        /// <summary>
        /// TODO Don't allow this to be running more than once at a time.
        /// </summary>
        public async Task GenerateWebsite()
        {
        
            await Task.Run(() => _website.GenerateWebsite(_config.Output));
        }

        public void MakeNewLogForNextWebsiteGenerationRun()
        {
            string nowTime = DateTime.Now.ToString("F");
            WebsiteRunLogs.Add(nowTime, new ObservableCollection<string>());
            CurrentWebsiteRunLog = WebsiteRunLogs[nowTime];
        }

        public void StartColorTemplateFileWatcher() =>
            _fileWatcher.StartColorTemplateWatch(
                Config.CSSSourcePath,
                CurrentlySelectedColorTemplate);

        public void StopColorTemplateFileWatcher() => _fileWatcher.StopColorTemplateWatch();

        // TODO : make this safe (both UI and threading)
        public void DownloadLevelismBlog(string url) => Task.Run(() =>
            _levelismConverter.DownloadBlog(url, Config.BlogPath, _config.LevelismBlogPagesToSnake));

        // dont currently need this
        //private void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        // => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        public event PropertyChangedEventHandler PropertyChanged;

        private readonly FileWatcher _fileWatcher;
        private readonly ILoggingService _loggingService;
        private readonly LevelismBlogMarkdownConverter _levelismConverter;
        private readonly WebsiteGenerator _website;
        private readonly Configuration _config;
        private string _logFilter = "Info";
        private string _currentlySelectedColorTemplate;
    }
}