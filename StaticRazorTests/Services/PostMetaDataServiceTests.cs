﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StaticRazor.Services;
using System.IO;

namespace StaticRazorTests
{
    [TestClass]
    public class PostMetaDataServiceTests
    {
        [TestMethod]
        public void Test_ExtractPostMetaDataJsonFragment01()
        {
            var postMetaDataService = new MetaDataParsingService();
            var input = File.ReadAllText(
                @"Services\PostMetaDataServiceTests_Data\Test_ExtractPostMetaDataJsonFragment01_Input.txt");
            var expectedResult = File.ReadAllText(
                @"Services\PostMetaDataServiceTests_Data\Test_ExtractPostMetaDataJsonFragment01_ExpectedResult.txt");
            var extractedValue = postMetaDataService.ExtractMetaDataJsonFragment(input);

            Assert.IsTrue(extractedValue == expectedResult);
        }
    }
}